package br.com.spotdev.teslapattern.fx2d;

import java.awt.*;

import javax.swing.*;
import javax.swing.text.Document;

@SuppressWarnings("serial")
public class PlaceholderTextField extends JTextField {

 
    private String placeholder;

    public PlaceholderTextField() {
    	this.setFont(new Font("Arial",Font.BOLD,50));
    	this.setForeground(Color.WHITE);
    	this.setBackground(new Color(0,0,0,140));
    	this.setBorder(null);
    }

    public PlaceholderTextField(
        final Document pDoc,
        final String pText,
        final int pColumns)
    {
        super(pDoc, pText, pColumns);
    }

    public PlaceholderTextField(final int pColumns) {
        super(pColumns);
    }

    public PlaceholderTextField(final String pText) {
        super(pText);
    }

    public PlaceholderTextField(final String pText, final int pColumns) {
        super(pText, pColumns);
    }

    public String getPlaceholder() {
        return placeholder;
    }

    @Override
    protected void paintComponent(final Graphics pG) {
        super.paintComponent(pG);
        
        double frameHeigth = pG.getClip().getBounds().getHeight();
        final float centerY = (int)frameHeigth/2;
        
        pG.setColor(Color.WHITE);

        if (placeholder == null || placeholder.length() == 0 || getText().length() > 0) {
            return;
        }

        final Graphics2D g = (Graphics2D) pG;
        g.setColor(getDisabledTextColor());
        g.setFont(new Font("Arial",Font.BOLD,50));
        g.drawString(placeholder, 10, centerY+getFontMetrics(getFont()).getHeight()/2-10);
    }

    public void setPlaceholder(final String s) {
        placeholder = s;
    }

}