package br.com.spotdev.teslapattern.fx3d;

import br.com.spotdev.teslapattern.fx3d.player.MoveKeyFrame;
import br.com.spotdev.teslapattern.fx3d.player.PlayerCamera;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.AmbientLight;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.PointLight;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.shape.Sphere;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;

public class World extends Xform {

	final Group axisGroup = new Group();
	final Group light = new Group();

	private static final double CAMERA_INITIAL_X_ANGLE = 0;
	private static final double CAMERA_INITIAL_Y_ANGLE = 0;

	private static final double MOUSE_SPEED = 0.1;
	private static final double ROTATION_SPEED = 2.0;
	private static final double TRACK_SPEED = 0.3;

	double mousePosX;
	double mousePosY;
	double mouseOldX;
	double mouseOldY;
	double mouseDeltaX;
	double mouseDeltaY;
	
	Stage stage = null;
	
	PlayerCamera pCamera = null;
	MoveKeyFrame moveKeyFrame = null;
	
	public World(Stage stage) {
		this.getChildren().add(new AmbientLight(Color.WHITE));

		pCamera = new PlayerCamera();
		this.stage = stage;
		moveKeyFrame = new MoveKeyFrame(pCamera);
		moveKeyFrame.play();
		
		buildAxes();
        
        AmbientLight al = new AmbientLight(Color.WHITE);
        PointLight pl = new PointLight();
        Sphere lamp = new Sphere(100);

        this.getChildren().add(light);
        light.getChildren().add(pl);
        light.getChildren().add(al);
        light.getChildren().add(lamp);
		
	}
	
	public void addWorldEvents(Scene scene) {
		scene.setCamera(pCamera.getCamera());

		handleMouse(scene, this);
		handleKeyboard(scene, this);
		
        stage.iconifiedProperty().addListener(
        		new ChangeListener<Boolean>() {
    	  @Override public void changed(ObservableValue<? extends Boolean> prop,
    			  Boolean oldValue, Boolean newValue) {
    		  moveKeyFrame.stopAll();
		  }
		});
	}
	
	 public Group getLight() {
		return light;
	}



	private void buildAxes() {
	        final PhongMaterial redMaterial = new PhongMaterial();
	        redMaterial.setDiffuseColor(Color.DARKRED);
	        redMaterial.setSpecularColor(Color.RED);
	 
	        final PhongMaterial greenMaterial = new PhongMaterial();
	        greenMaterial.setDiffuseColor(Color.DARKGREEN);
	        greenMaterial.setSpecularColor(Color.GREEN);
	 
	        final PhongMaterial blueMaterial = new PhongMaterial();
	        blueMaterial.setDiffuseColor(Color.DARKBLUE);
	        blueMaterial.setSpecularColor(Color.BLUE);
	 
	        final Box xAxis = new Box(240.0, 1, 1);
	        final Box yAxis = new Box(1, 240.0,1);
	        final Box zAxis = new Box(1, 1, 240.0);
	        Translate translate = new Translate(20, 0, 500);
	        
	        xAxis.setMaterial(redMaterial);
	        yAxis.setMaterial(greenMaterial);
	        zAxis.setMaterial(blueMaterial);
	        
	        xAxis.getTransforms().add(translate);
	        yAxis.getTransforms().add(translate);
	        zAxis.getTransforms().add(translate);
	 
	        //axisGroup.getChildren().addAll(xAxis, yAxis, zAxis);
	        this.getChildren().addAll(xAxis, yAxis, zAxis);
	    }

	
	private void handleMouse(Scene scene, final Node root) {
		
		scene.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override public void handle(MouseEvent me) {
				mousePosX = me.getSceneX();
				mousePosY = me.getSceneY();
				mouseOldX = me.getSceneX();
				mouseOldY = me.getSceneY();

			}
		});
		scene.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override public void handle(MouseEvent me) {
				mouseOldX = mousePosX;
				mouseOldY = mousePosY;
				mousePosX = me.getSceneX();
				mousePosY = me.getSceneY();
				mouseDeltaX = (mousePosX - mouseOldX);
				mouseDeltaY = (mousePosY - mouseOldY);
			    double modifierFactor = 0.1;
				double modifier = 1.0;
				Xform movable = getpCamera().getCameraXform3();
				Xform cameraXform2 = getpCamera().getCameraXform2();
				if (me.isMiddleButtonDown() || me.isControlDown()) {
					double z = getpCamera().getCamera().getTranslateZ();
					double newZ = z + mouseDeltaX*MOUSE_SPEED*modifier*20;
					getpCamera().getCamera().setTranslateZ(newZ);
				}
				
				else if (me.isPrimaryButtonDown() && getpCamera().isFreezed() == true) {
					movable.ry.setAngle(movable.ry.getAngle() +
							mouseDeltaX*modifierFactor*modifier*ROTATION_SPEED); //
					movable.rx.setAngle(movable.rx.getAngle() -
							mouseDeltaY*modifierFactor*modifier*ROTATION_SPEED); // -
				}
				else if (me.isSecondaryButtonDown()) {
					cameraXform2.t.setX(cameraXform2.t.getX() +
							mouseDeltaX*MOUSE_SPEED*modifier*6*TRACK_SPEED); // -
					cameraXform2.t.setY(cameraXform2.t.getY() +
							mouseDeltaY*MOUSE_SPEED*modifier*6*TRACK_SPEED); // -
				}
			}
		}); // setOnMouseDragged
	} // handleMouse

	private void handleKeyboard(Scene scene, final Node root) {
		scene.setOnKeyReleased(new EventHandler<KeyEvent>() {

			@Override
			public void handle(KeyEvent event) {
				if(event.getCode() == KeyCode.W) {
					moveKeyFrame.getUpPressed().set(false);
				}else if(event.getCode() == KeyCode.D) {
					moveKeyFrame.getRightPressed().set(false);
				} else if(event.getCode() == KeyCode.S) {	
					moveKeyFrame.getDownPressed().set(false);	
				} else if(event.getCode() == KeyCode.A) {	
					moveKeyFrame.getLeftPressed().set(false);	
				} else if(event.getCode() == KeyCode.SPACE) {
					moveKeyFrame.getSpacePressed().set(false);				
				} else if(event.getCode() == KeyCode.SHIFT) {
					moveKeyFrame.getShiftPressed().set(false);			
				}
				
				
			}
		});
		scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				switch (event.getCode()) {
				case Z:
					getpCamera().getCameraXform2().t.setX(0.0);
					getpCamera().getCameraXform2().t.setY(0.0);
					getpCamera().getCameraXform().ry.setAngle(CAMERA_INITIAL_Y_ANGLE);
					getpCamera().getCameraXform().rx.setAngle(CAMERA_INITIAL_X_ANGLE);
					break;
//				case V:
//					matrix.setVisible(!matrix.isVisible());
//					System.out.println(matrix.isVisible());
//					break;
				default:
					break;
				} // switch
				
				if(event.getCode() == KeyCode.W) {
					moveKeyFrame.getUpPressed().set(true);
				}else if(event.getCode() == KeyCode.S) {	
					moveKeyFrame.getDownPressed().set(true);	
				}
				
				if(event.getCode() == KeyCode.D) {
					moveKeyFrame.getRightPressed().set(true);
				} else if(event.getCode() == KeyCode.A) {
					moveKeyFrame.getLeftPressed().set(true);
				}
				
				if(event.getCode() == KeyCode.SPACE) {
					moveKeyFrame.getSpacePressed().set(true);				
				} else if(event.getCode() == KeyCode.SHIFT) {
					moveKeyFrame.getShiftPressed().set(true);			
				}
			} // handle()
		}); // setOnKeyPressed
	} // handleKeybF

	public double getMousePosX() {
		return mousePosX;
	}

	public void setMousePosX(double mousePosX) {
		this.mousePosX = mousePosX;
	}

	public double getMousePosY() {
		return mousePosY;
	}

	public void setMousePosY(double mousePosY) {
		this.mousePosY = mousePosY;
	}

	public double getMouseOldX() {
		return mouseOldX;
	}

	public void setMouseOldX(double mouseOldX) {
		this.mouseOldX = mouseOldX;
	}

	public double getMouseOldY() {
		return mouseOldY;
	}

	public void setMouseOldY(double mouseOldY) {
		this.mouseOldY = mouseOldY;
	}

	public double getMouseDeltaX() {
		return mouseDeltaX;
	}

	public void setMouseDeltaX(double mouseDeltaX) {
		this.mouseDeltaX = mouseDeltaX;
	}

	public double getMouseDeltaY() {
		return mouseDeltaY;
	}

	public void setMouseDeltaY(double mouseDeltaY) {
		this.mouseDeltaY = mouseDeltaY;
	}

	public PlayerCamera getpCamera() {
		return pCamera;
	}

	public void setpCamera(PlayerCamera pCamera) {
		this.pCamera = pCamera;
	}

	public MoveKeyFrame getMoveKeyFrame() {
		return moveKeyFrame;
	}

	public void setMoveKeyFrame(MoveKeyFrame moveKeyFrame) {
		this.moveKeyFrame = moveKeyFrame;
	}

	public Group getAxisGroup() {
		return axisGroup;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}


	
	
	
}
