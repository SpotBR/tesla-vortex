package br.com.spotdev.teslapattern.fx3d.player;

import br.com.spotdev.teslapattern.fx3d.Xform;
import br.com.spotdev.teslapattern.fx3d.shapes.Matrix3D;
import javafx.scene.PerspectiveCamera;

public class PlayerCamera {
	
	final PerspectiveCamera camera = new PerspectiveCamera(true);
	final Xform cameraXform = new Xform();
	final Xform cameraXform2 = new Xform();
	final Xform cameraXform3 = new Xform();
	
	private boolean freezed = true;
	
	private static final double CAMERA_NEAR_CLIP = 0.1;
	private static final double CAMERA_FAR_CLIP = 12000050.0;
	
	public Xform buildCamera(Matrix3D matrix) {
		cameraXform.getChildren().add(cameraXform2);
		cameraXform2.getChildren().add(cameraXform3);
		cameraXform3.getChildren().add(camera);
		cameraXform3.setRotateZ(180.0);
		cameraXform3.setRotateX(-35);
		cameraXform3.setRotateY(45.0);
		camera.setNearClip(CAMERA_NEAR_CLIP);

		camera.setFarClip(CAMERA_FAR_CLIP);
		cameraXform3.setTranslateZ(cameraXform3.getTranslateZ()-matrix.getTotalPixelsDepth());
		cameraXform3.setTranslateX(matrix.getTotalPixelsWidth()+matrix.getTotalPixelsWidth());
		cameraXform3.setTranslateY(matrix.getTotalPixelsHeight()+matrix.getTotalPixelsHeight());
		return cameraXform;
	}

	public PerspectiveCamera getCamera() {
		return camera;
	}

	public Xform getCameraXform() {
		return cameraXform;
	}

	public Xform getCameraXform2() {
		return cameraXform2;
	}

	public Xform getCameraXform3() {
		return cameraXform3;
	}

	public boolean isFreezed() {
		return freezed;
	}

	public void setFreezed(boolean freezed) {
		this.freezed = freezed;
	}
	
	
	
}