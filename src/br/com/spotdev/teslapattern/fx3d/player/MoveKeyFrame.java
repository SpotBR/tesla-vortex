package br.com.spotdev.teslapattern.fx3d.player;
import br.com.spotdev.teslapattern.fx3d.Xform;
import br.com.spotdev.teslapattern.fx3d.shapes.Utils;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.util.Duration;

public class MoveKeyFrame {

	private static final Duration MOVE_DELAY = Duration.millis(10);
	private static final float MOVE_DISTANCE = 100;
	
	private Timeline timer = new Timeline(new KeyFrame(MOVE_DELAY, 
			this::handleMovementTimer));
	
	private BooleanProperty upPressed = new SimpleBooleanProperty();
	private BooleanProperty rightPressed = new SimpleBooleanProperty();
	private BooleanProperty downPressed = new SimpleBooleanProperty();
	private BooleanProperty leftPressed = new SimpleBooleanProperty();
	private BooleanProperty shiftPressed = new SimpleBooleanProperty();
	private BooleanProperty spacePressed = new SimpleBooleanProperty();
	private PlayerCamera pCamera = null;
	
	public MoveKeyFrame(PlayerCamera pCamera) {
		timer.setCycleCount(Timeline.INDEFINITE);//Set to run Timeline forever or as long as the app is running.
		this.pCamera = pCamera;
	}
	
	public void play() {
		timer.play();
	}

	public void handleMovementTimer(ActionEvent e) {

		Xform movable = getpCamera().getCameraXform();
		float angle = Utils.getAngleWithoutZero(getpCamera().getCameraXform3().ry);
		float radianCos = (float) Math.cos(Math.toRadians(angle));
		float radianSin = (float) Math.sin(Math.toRadians(angle));
		float dx = MOVE_DISTANCE * radianSin;
		float dz = MOVE_DISTANCE * radianCos;
		if(upPressed.get()) {
			movable.setTranslateZ(movable.getTranslateZ()+MOVE_DISTANCE * 
					Math.cos(Math.toRadians(angle)));
			movable.setTranslateX(movable.getTranslateX()-MOVE_DISTANCE * 
					Math.sin(Math.toRadians(angle)));
		}
		
		if(downPressed.get()) {
			movable.setTranslateZ(movable.getTranslateZ()-dz);
			movable.setTranslateX(movable.getTranslateX()+dx);	
		}
		
		if(leftPressed.get()) {
			movable.setTranslateZ(movable.getTranslateZ()-Math.cos(Math.toRadians(angle+90))*MOVE_DISTANCE);
			movable.setTranslateX(movable.getTranslateX()+Math.sin(Math.toRadians(angle+90))*MOVE_DISTANCE);
		}
		
		if(rightPressed.get()) {
			movable.setTranslateZ(movable.getTranslateZ()+Math.cos(Math.toRadians(angle+90))*MOVE_DISTANCE);
			movable.setTranslateX(movable.getTranslateX()-Math.sin(Math.toRadians(angle+90))*MOVE_DISTANCE);			
		}
		
		if(spacePressed.get()) {
			movable.setTranslateY(movable.getTranslateY()+MOVE_DISTANCE);
		}
		
		if(shiftPressed.get()) {
			movable.setTranslateY(movable.getTranslateY()-MOVE_DISTANCE);
		}
	}
	
	public void stopAll() {
		upPressed.set(false);
		downPressed.set(false);
		leftPressed.set(false);
		rightPressed.set(false);
		spacePressed.set(false);
		shiftPressed.set(false);
	}
	
	public Timeline getTimer() {
		return timer;
	}

	public void setTimer(Timeline timer) {
		this.timer = timer;
	}

	public PlayerCamera getpCamera() {
		return pCamera;
	}

	public void setpCamera(PlayerCamera pCamera) {
		this.pCamera = pCamera;
	}

	public BooleanProperty getUpPressed() {
		return upPressed;
	}

	public void setUpPressed(BooleanProperty upPressed) {
		this.upPressed = upPressed;
	}

	public BooleanProperty getRightPressed() {
		return rightPressed;
	}

	public void setRightPressed(BooleanProperty rightPressed) {
		this.rightPressed = rightPressed;
	}

	public BooleanProperty getDownPressed() {
		return downPressed;
	}

	public void setDownPressed(BooleanProperty downPressed) {
		this.downPressed = downPressed;
	}

	public BooleanProperty getLeftPressed() {
		return leftPressed;
	}

	public void setLeftPressed(BooleanProperty leftPressed) {
		this.leftPressed = leftPressed;
	}

	public BooleanProperty getShiftPressed() {
		return shiftPressed;
	}

	public void setShiftPressed(BooleanProperty shiftPressed) {
		this.shiftPressed = shiftPressed;
	}

	public BooleanProperty getSpacePressed() {
		return spacePressed;
	}

	public void setSpacePressed(BooleanProperty spacePressed) {
		this.spacePressed = spacePressed;
	}
	
}
