package br.com.spotdev.teslapattern.fx3d.shapes;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Cylinder;

public class AbstractMatrix3D {

	protected int width;
	protected int height;
	protected int depth;
	protected float cubeSideLenght;
	
	private Group lineGroup = null;
	
	public static final PhongMaterial LINE_MATERIAL = new PhongMaterial();
	static {
		//LINE_MATERIAL.diffuseMapProperty();
		LINE_MATERIAL.setDiffuseColor(new Color(0,0,0,0.03));  // Note alpha of 0.6
		LINE_MATERIAL.diffuseMapProperty();
	}

	public static final PhongMaterial RED_MATERIAL = new PhongMaterial();
	static {
		//LINE_MATERIAL.diffuseMapProperty();
		
		RED_MATERIAL.setDiffuseColor(Color.RED);  // Note alpha of 0.6
		RED_MATERIAL.setSpecularColor(Color.INDIANRED);
		RED_MATERIAL.diffuseMapProperty();
	}
	
	public static final PhongMaterial MARK_MATERIAL = new PhongMaterial();
	static {
		//LINE_MATERIAL.diffuseMapProperty();
		MARK_MATERIAL.setDiffuseColor(Color.CADETBLUE);  // Note alpha of 0.6
		MARK_MATERIAL.setSpecularColor(Color.AQUA);
		MARK_MATERIAL.diffuseColorProperty();  

	}
	
	
	enum MatrixType {
		CUBES,
		FLOWER_OF_LIFE,
		SQUARE_TABLE,
		SPHERES
	}

	public List<Node> blocksCreatedList = new ArrayList<Node>();
	private MatrixType matrixType = MatrixType.CUBES;
	private ObservableList<Node> parent = null;
	
	public AbstractMatrix3D(int width, int height, int depth, float cubeSideLenght, ObservableList<Node> parent) {
		this.width = width;
		this.height = height;
		this.depth = depth;
		this.cubeSideLenght = cubeSideLenght;
		this.parent = parent;
		
		lineGroup = new Group();
		parent.add(lineGroup);
		
	}
	
	public ObservableList<Node> getParent() {
		return parent;
	}



	public void setParent(ObservableList<Node> parent) {
		this.parent = parent;
	}



	public List<Node> getBlocksCreatedList() {
		return blocksCreatedList;
	}



	public void setBlocksCreatedList(List<Node> blocksCreatedList) {
		this.blocksCreatedList = blocksCreatedList;
	}



	public MatrixType getMatrixType() {
		return matrixType;
	}



	public void setMatrixType(MatrixType matrixType) {
		this.matrixType = matrixType;
	}



	public int getWidth() {
		return width;
	}

	public float getTotalPixelsWidth() {
		return width*getCubeSideLenght();
	}
	
	public float getTotalPixelsDepth() {
		return depth*getCubeSideLenght();
	}
	
	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public float getTotalPixelsHeight() {
		return height*getCubeSideLenght();
	}
	
	public void setHeight(int height) {
		this.height = height;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public float getCubeSideLenght() {
		return cubeSideLenght;
	}

	public void setCubeSideLenght(float cubeSideLenght) {
		this.cubeSideLenght = cubeSideLenght;
	}

	public Group getLineGroup() {
		return lineGroup;
	}

	public void setLineGroup(Group lineGroup) {
		this.lineGroup = lineGroup;
	}

	
	
	
}
