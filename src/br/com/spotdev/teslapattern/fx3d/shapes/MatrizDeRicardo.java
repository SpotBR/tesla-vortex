package br.com.spotdev.teslapattern.fx3d.shapes;

import java.util.HashMap;
import br.com.spotdev.teslapattern.fx3d.shapes.Matrix3D.BlockOutOfMatrixException;
import javafx.geometry.Point3D;

public class MatrizDeRicardo {

	public static void generate(Matrix3D m) {
		
//		int v = 0;
//		GregorianCalendar gc = new GregorianCalendar(2019, 2, 25);
//		SimpleDateFormat sf = new SimpleDateFormat();
//		sf.format(gc)
//		
//		for(int i = 0; i < 100; i++) {
//			v+=i;
//			gc.add(GregorianCalendar.DAY_OF_MONTH, 1);
//			System.out.println();
//		}
		
		double RADIUSUsed = 5;
		try {
			for(int i = 0; i < RADIUSUsed; i++) {
				m.addBlock(i*-1, 0, 
						0, "", Matrix3D.RED_MATERIAL);
				m.addBlock(i, 0, 
						0, "", Matrix3D.RED_MATERIAL);
				m.addBlock(0, i*-1, 
						0, "", Matrix3D.RED_MATERIAL);
				m.addBlock(0, i, 
						0, "", Matrix3D.RED_MATERIAL);
			}
			
		} catch (BlockOutOfMatrixException e1) {
		}
		// Eu precisava que o pi não só pre calculasse corretamente em uma matriz quadrada
		// como também pulasse para a s
		java.util.Map<Point3D,Point3D> usedPoints = new HashMap<>();
		double pi = 3.14;
		double COLUMN_LENGHT =1001;
		System.out.println(COLUMN_LENGHT);
		for (int cn = 1; cn <= COLUMN_LENGHT; cn++) {
			double xval = (RADIUSUsed
                    * Math.cos(pi/4 / COLUMN_LENGHT * cn));
            double yval = (RADIUSUsed
                    * Math.sin(pi/4 / COLUMN_LENGHT * cn));
            try {
//            	System.out.println(Math.cos(cn * 2 * 3.2 / COLUMN_LENGHT));
            	Point3D p = new Point3D(Math.round(xval), Math.round(yval), 0);
            	if(!usedPoints.containsKey(p)) {
            		usedPoints.put(p, new Point3D(xval, yval, 0));
    				m.addBlock((int)Math.round(xval), (int)Math.round(yval), 
    						0, cn+"", Matrix3D.MARK_MATERIAL);
            	}
//            	System.out.println(xval,yval)
//				System.out.println(cn+" "+xval+" - "+p.getX()+" | "+yval+" - "+p.getY());
			} catch (BlockOutOfMatrixException e) {}
		
		}
		int i = 0;
		for(Point3D k : usedPoints.keySet()) {
			i++;
			System.out.println(i+" - "+k+"  | "+usedPoints.get(k));
		}
		
		  
				
		//m.getParent().add(m.createConnection(new Point3D(0, 0, 0), new Point3D(90*m.getCubeSideLenght(), 90*m.getCubeSideLenght(), 90*m.getCubeSideLenght())));
//		Point3D zero = new Point3D(0, 0, 0);
//		for(int y = 0; y < m.getHeight(); y++) {
//			for(int z = 0; z < m.getDepth(); z++) {
//				for(int x = 0; x < m.getWidth(); x++) {
//					Point3D p = new Point3D(x*y, y, z*y);
//					try {
//						m.addBlock((int)p.getX(), (int)p.getY(), 
//								(int)p.getZ(), (y)*(x)+" - "+y+" - "+z*y);
//					} catch (BlockOutOfMatrixException e) {}
////
////					Cylinder c = m.createConnection(zero, p);
////					m.getParent().add(c);
//				}
//			}
//			
//		}
		
//		for(int y = 0; y < 30; y++) {
//			for(int z = y; z < y+9; z++) {
//				Point3D p = new Point3D(y, y, z*y);
//				try {
//					m.addBlock((int)p.getX(), (int)p.getY(), 
//							(int)p.getZ(), z*y+"");
//					m.addBlock((int)p.getX()*-1, (int)p.getY(), 
//							(int)p.getZ(), z*y+"");
//				} catch (BlockOutOfMatrixException e) {}
////
////					Cylinder c = m.createConnection(zero, p);
////					m.getParent().add(c);
//			
//			}
//			
//		}
//		
//		Quero que o i seja:
//		0			0
//		1			1
//		depois 3	2
//		depois 6	3	3
//		depois 10	4	1
//		depois 15	5	6
//		depois 21	6	3
//		depois 28	7	1
//		depois 36	8	9
//		depois 45	9	9
//		depois 55	10	1
//		depois 66	11	3
//		depois 78	12	3
//		depois 91	13	1
//		depois 105  14	6
//		depois 120	15	3
//		depois 
//		int i2 = 0;

//		O problema do índice chegar nesses valores,
//		é que você precisa acumular ele, sem alterar o espaço
//		que também é o próprio indice.
//		Para funcionar seria preciso acumular o espaço e o histórico 
//		do espaço, em uma mesma equação
//		Por que você não queria usar dois loops e nem a variável externa?
//		Usar a variável externa é bem esquisito pois é carregar um peso a mais
//		por algo que por instinto me parece muito óbvio
//		Se tudo tem um padrão ali bem claro, deveria ser óbvio qual o número que eu quero
//		  pela quantidade de espaço
//		int xi[][] = new int[10][10];
//		int indexToSum = 0;
//		for(int x = 0; x < xi.length; x++) {
//			for(int i = 1; i < xi[x].length; i++) {
//				indexToSum++;
//				i2+=indexToSum;
//				int xLenght = x*10;
//				System.out.print(i2+" ("+(i+xLenght)+")"+" - ");
//				xi[x][i] = i2;
//
//			}
//			System.out.print(" - ");
//			if(x > 0) {
//				for(int i = 1; i < xi[x].length; i++) {
//					System.out.print((xi[x][i]-xi[x-1][i])+"|");
//				}
//			}
//			System.out.println(" - ");
		
//		for(int i = 0; i < 100; i++) {
//			System.out.print(sumDigit(fibo(i))+"-");
//		}
		
	}	
	
	
    public static int fibo(int n) {
        if (n < 2) {
            return n;
        } else {
            return fibo(n - 1) + fibo(n - 2);
        }
    }
	
    private static int sumDigit(int digit) {
    	String dS[] = (digit+"").split("");
    	if(dS.length > 1) {
	    	int result = 0;
	    	while(result == 0 || result >= 10) {
	    		result = 0;
	    		for(int i = 0; i < dS.length; i++) {
	    			result+=Integer.parseInt(dS[i]);
	    		}
    	    	dS = (result+"").split("");
	    	}
	    	return result;
    	}else {
    		return Integer.parseInt(dS[0]);
    	}
    }
	
}
// A última posição, que no inicio é sempre zero, precisa ser lembrada, para você somar à ela o valor do espaçamento. No primeiro não tem última posição. Por isso a 