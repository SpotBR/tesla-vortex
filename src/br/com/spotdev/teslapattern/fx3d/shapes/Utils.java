package br.com.spotdev.teslapattern.fx3d.shapes;

import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Point3D;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.TriangleMesh;
import javafx.scene.transform.Rotate;

public class Utils {

	public static float getAngleWithoutZero(Rotate r) {
		return (float)( r.getAngle() == 0 ?
				360 : r.getAngle());
	}
	

//	public static List<Cylinder> createToroidCylinders(float radius, float tRadius, 
//			int tubeDivisions, int radiusDivisions) {
//		List<Cylinder> c = new ArrayList<Cylinder>();
//		int POINT_SIZE = 3, TEXCOORD_SIZE = 2, FACE_SIZE = 6;
//		int numVerts = tubeDivisions * radiusDivisions;
//		int faceCount = numVerts * 2;
//		float[] points = new float[numVerts * POINT_SIZE], texCoords = new float[numVerts * TEXCOORD_SIZE];
//		int[] faces = new int[faceCount * FACE_SIZE], smoothingGroups;
//
//		int pointIndex = 0, texIndex = 0, faceIndex = 0, smoothIndex = 0;
//		float tubeFraction = 1.0f / tubeDivisions;
//		float radiusFraction = 1.0f / radiusDivisions;
//		float x, y, z;
//
//		int p0 = 0, p1 = 0, p2 = 0, p3 = 0, t0 = 0, t1 = 0, t2 = 0, t3 = 0;
//
//		// create points
//		int i = 0;
//		PhongMaterial redMaterial = new PhongMaterial();
//		 redMaterial.setSpecularColor(Color.ORANGE);
//		 redMaterial.setDiffuseColor(Color.RED);
//		for (int tubeIndex = 0; tubeIndex < tubeDivisions; tubeIndex++) {
//
//			float radian = (float) (tubeFraction * tubeIndex * 2.0f * Math.PI);
//			float nextRadian = (float) (tubeFraction * (tubeIndex+1) * 2.0f * Math.PI);
//
//			for (int radiusIndex = 0; radiusIndex < radiusDivisions; radiusIndex++) {
//				i++;
//				float localRadian = (float) (radiusFraction * radiusIndex * 2.0f * Math.PI);
//
//				points[pointIndex] = x = (radius + tRadius * ((float) Math.cos(radian)))
//						* ((float) Math.cos(localRadian));
//				points[pointIndex + 1] = y = (radius + tRadius * ((float) Math.cos(radian)))
//						* ((float) Math.sin(localRadian));
//				float nextZ = (tRadius * (float) Math.sin(nextRadian));
//				points[pointIndex + 2] = z = ((nextZ-(tRadius*(float)Math.sin(radian)))/radiusDivisions*(radiusIndex+1))+tRadius * (float) Math.sin(radian);
//				
//				if(pointIndex >= 3) {
//					i = 0;
//					final Cylinder cy = Matrix3D.createConnection(new Point3D(x, y, z), 
//								new Point3D(points[pointIndex-3], 
//										points[pointIndex-2], points[pointIndex-1]));
//					
//					if(radiusIndex == 0) {
//						cy.setMaterial(redMaterial);
//						System.out.println(radiusIndex+" | "+((nextZ-(tRadius*(float)Math.sin(radian)))/radiusDivisions)+" | "+points[pointIndex-3]+" - "+ 
//								points[pointIndex-2]+" - "+ points[pointIndex-1]+" |  "+x+" - "+y+" - "+z);
//					}else {
//
//						System.out.println("\t\t"+radiusIndex+" | "+((nextZ-(tRadius*(float)Math.sin(radian)))/radiusDivisions)+" |"+points[pointIndex-3]+" - "+ 
//								points[pointIndex-2]+" - "+ points[pointIndex-1]+" |  "+x+" - "+y+" - "+z);
//					}
//					c.add(cy);
//				}
//
//				pointIndex += 3;
//				float r = radiusIndex < tubeDivisions ? tubeFraction * radiusIndex * 2.0F * 3.141592653589793f : 0.0f;
//				texCoords[texIndex] = (0.5F + (float) (Math.sin(r) * 0.5D));
//				
//				texCoords[texIndex + 1] = ((float) (Math.cos(r) * 0.5D) + 0.5F);
//
//				texIndex += 2;
//			}
//
//		}
//
//		
//		return c;
//	}
	

	
	public static TriangleMesh createToroidMesh(float radius, float tRadius, 
			int tubeDivisions, int radiusDivisions) {
		
		int POINT_SIZE = 3, TEXCOORD_SIZE = 2, FACE_SIZE = 6;
		int numVerts = tubeDivisions * radiusDivisions;
		int faceCount = numVerts * 2;
		float[] points = new float[numVerts * POINT_SIZE], texCoords = new float[numVerts * TEXCOORD_SIZE];
		int[] faces = new int[faceCount * FACE_SIZE], smoothingGroups;

		int pointIndex = 0, texIndex = 0, faceIndex = 0, smoothIndex = 0;
		float tubeFraction = 1.0f / tubeDivisions;
		float radiusFraction = 1.0f / radiusDivisions;
		float x, y, z;

		int p0 = 0, p1 = 0, p2 = 0, p3 = 0, t0 = 0, t1 = 0, t2 = 0, t3 = 0;

		// create points
		for (int tubeIndex = 0; tubeIndex < tubeDivisions; tubeIndex++) {

			float radian = tubeFraction * tubeIndex * 2.0f * 3.141592653589793f;

			for (int radiusIndex = 0; radiusIndex < radiusDivisions; radiusIndex++) {

				float localRadian = radiusFraction * radiusIndex * 2.0f * 3.141592653589793f;

				points[pointIndex] = x = (radius + tRadius * ((float) Math.cos(radian)))
						* ((float) Math.cos(localRadian));
				points[pointIndex + 1] = y = (radius + tRadius * ((float) Math.cos(radian)))
						* ((float) Math.sin(localRadian));
				points[pointIndex + 2] = z = (tRadius * (float) Math.sin(radian));

				pointIndex += 3;

				float r = radiusIndex < tubeDivisions ? tubeFraction * radiusIndex * 2.0F * 3.141592653589793f : 0.0f;
				texCoords[texIndex] = (0.5F + (float) (Math.sin(r) * 0.5D));
				
				texCoords[texIndex + 1] = ((float) (Math.cos(r) * 0.5D) + 0.5F);

				texIndex += 2;
			}

		}
		
		// create faces
		for (int point = 0; point < (tubeDivisions); point++) {
			for (int crossSection = 0; crossSection < (radiusDivisions); crossSection++) {
				p0 = point * radiusDivisions + crossSection;
				p1 = p0 >= 0 ? p0 + 1 : p0 - (radiusDivisions);
				p1 = p1 % (radiusDivisions) != 0 ? p0 + 1 : p0 - (radiusDivisions - 1);
				p2 = (p0 + radiusDivisions) < ((tubeDivisions * radiusDivisions)) ? p0 + radiusDivisions
						: p0 - (tubeDivisions * radiusDivisions) + radiusDivisions;
				p3 = p2 < ((tubeDivisions * radiusDivisions) - 1) ? p2 + 1 : p2 - (tubeDivisions * radiusDivisions) + 1;
				p3 = p3 % (radiusDivisions) != 0 ? p2 + 1 : p2 - (radiusDivisions - 1);

				t0 = point * (radiusDivisions) + crossSection;
				t1 = t0 >= 0 ? t0 + 1 : t0 - (radiusDivisions);
				t1 = t1 % (radiusDivisions) != 0 ? t0 + 1 : t0 - (radiusDivisions - 1);
				t2 = (t0 + radiusDivisions) < ((tubeDivisions * radiusDivisions)) ? t0 + radiusDivisions
						: t0 - (tubeDivisions * radiusDivisions) + radiusDivisions;
				t3 = t2 < ((tubeDivisions * radiusDivisions) - 1) ? t2 + 1 : t2 - (tubeDivisions * radiusDivisions) + 1;
				t3 = t3 % (radiusDivisions) != 0 ? t2 + 1 : t2 - (radiusDivisions - 1);

				try {
					faces[faceIndex] = (p2);
					faces[faceIndex + 1] = (t3);
					faces[faceIndex + 2] = (p0);
					faces[faceIndex + 3] = (t2);
					faces[faceIndex + 4] = (p1);
					faces[faceIndex + 5] = (t0);

					faceIndex += FACE_SIZE;

					faces[faceIndex] = (p2);
					faces[faceIndex + 1] = (t3);
					faceIndex += FACE_SIZE;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}	

		TriangleMesh localTriangleMesh = new TriangleMesh();
		localTriangleMesh.getPoints().setAll(points);
		localTriangleMesh.getTexCoords().setAll(texCoords);
		localTriangleMesh.getFaces().setAll(faces);

		return localTriangleMesh;
	}
	
}
