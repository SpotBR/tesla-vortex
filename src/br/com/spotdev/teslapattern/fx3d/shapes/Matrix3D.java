package br.com.spotdev.teslapattern.fx3d.shapes;

import javafx.collections.ObservableList;
import javafx.geometry.Point3D;
import javafx.scene.Node;
import javafx.scene.control.Tooltip;
import javafx.scene.paint.Material;
import javafx.scene.shape.Box;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.Shape3D;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;

public class Matrix3D extends AbstractMatrix3D {
	
	public Matrix3D(int width, int height, int depth, float cubeSideLenght, ObservableList<Node> group) {
		super(width, height, depth, cubeSideLenght, group);
	}
	
	public void generateGrid() {
		
		int startW = width/2*-1;
		int startH = height/2*-1;
		int startD = depth/2*-1;
		for(int x =startW; x <= width/2; x++) {	
			for(int y = startH; y <= height/2; y++) {	
				
				for(int z = startD; z <= depth; z++) {
					
					// PARA VOCÊ NÂO ESQUECER DENOVO, O COMANDO DEBAIXO É
					// PX.ADD, OU SEJA, ELE SOMA AO DE CIMA. NÃO É UMA NOVA INSTANCIA
					// Continuidade frontal
					Point3D px = new Point3D(x*cubeSideLenght, y*cubeSideLenght, startD*cubeSideLenght);
					Point3D px2 = px.add(0,0,cubeSideLenght*(float)depth);
					
					// Continuidade lateral
					Point3D pz = new Point3D(startW*cubeSideLenght, y*cubeSideLenght, z*cubeSideLenght);
					Point3D pz2 = pz.add(cubeSideLenght*(float)width,0,0);
					
					// Altura
					Point3D py = new Point3D(x*cubeSideLenght, startH*cubeSideLenght, z*cubeSideLenght);
					Point3D py2 = py.add(0,cubeSideLenght*(float)height,0);
					
					Cylinder c = createConnection(px, px2);
					Cylinder c2 = createConnection(pz, pz2);
					Cylinder c3 = createConnection(py, py2);
					
					getLineGroup().getChildren().add(c);
					getLineGroup().getChildren().add(c2);
					getLineGroup().getChildren().add(c3);
				}
			}
		}
		
		
//		Sphere redSphere = new Sphere(cubeSideLenght);
//		redSphere.getTransforms().add(new Translate(width/2*cubeSideLenght,
//				height/2*cubeSideLenght, depth/2*cubeSideLenght));	
//		redSphere.setMaterial(MARK_MATERIAL);
		//group.add(redSphere);
		
//		List<Sphere> esferas = new ArrayList<Sphere>();
//		for(int x = 0; x < width; x++) {
//			for(int y = 0; y < height; y++) {
//				for(int z = 0; z < depth; z++) {
//					Point3D py = new Point3D(x*(cubeSideLenght), y*(cubeSideLenght), 
//							z*(cubeSideLenght));
//					
//					PhongMaterial earthMaterial = new PhongMaterial();
//					
////			        Image earthImage = new Image("tex.jpeg");
////			        earthMaterial.setDiffuseMap(earthImage);
//			        earthMaterial.setDiffuseColor(new Color(0,0,0,0.03));  // Note alpha of 0.6
//			        earthMaterial.diffuseMapProperty();
//			        earthMaterial.setSpecularColor(Color.INDIANRED);      
//					
//					Sphere box = new Sphere(cubeSideLenght);
////					System.out.println(py.getX());
//					box.getTransforms().add(new Translate(py.getX(), py.getY(), py.getZ()));
//
//					box.setMaterial(earthMaterial);
//					esferas.add(box);
//
//				}
//			}
//		}
//		
//		
//		getParent().addAll(esferas.toArray(new Sphere[esferas.size()]));

	}
	
	public void setVisible(boolean v) {
		
		getLineGroup().setVisible(v);
	}
	
	public boolean isVisible() {
		return getLineGroup().isVisible();
	}
	
	public Cylinder createConnection(Point3D origin, Point3D target) {
//		origin = origin.add(origin.getX()*cubeSideLenght, origin.getY()*cubeSideLenght, origin.getZ()*cubeSideLenght);
//		target = target.add(target.getX()*cubeSideLenght, target.getY()*cubeSideLenght, target.getZ()*cubeSideLenght);
		Point3D yAxis = new Point3D(0, 1, 0);
	    Point3D diff = target.subtract(origin);
	    double height = diff.magnitude();

	    Point3D mid = target.midpoint(origin);
	    Translate moveToMidpoint = new Translate(mid.getX(), mid.getY(), mid.getZ());

	    Point3D axisOfRotation = diff.crossProduct(yAxis);
	    double angle = Math.acos(diff.normalize().dotProduct(yAxis));
	    Rotate rotateAroundCenter = new Rotate(-Math.toDegrees(angle), axisOfRotation);

	    Cylinder line = new Cylinder(2, height);
	    line.setMaterial(LINE_MATERIAL);

	    line.getTransforms().addAll(moveToMidpoint, rotateAroundCenter);

	    return line;
	}

	public void addBlock(int x, int y, int z, String toolTipText, Material m) throws BlockOutOfMatrixException {
//		if((x < 0 || x >= getWidth()) || (y < 0 || y > getHeight()) || (z < 0 || z >= getDepth()))
//			throw new BlockOutOfMatrixException(this);
		
		Shape3D node = null;
		switch(getMatrixType()) {
			case CUBES:
				float midCubeSid = cubeSideLenght/2;
				float centerX = x*cubeSideLenght+midCubeSid;
				float centerY = y*cubeSideLenght+midCubeSid;
				float centerZ = z*cubeSideLenght+midCubeSid;
				node = new Box(cubeSideLenght,cubeSideLenght,cubeSideLenght);
				Translate t = new Translate(centerX,centerY,centerZ);
				node.getTransforms().add(t);
				node.setMaterial(m);
				
				Tooltip tt = new Tooltip(toolTipText);
	            Tooltip.install(node, tt);
			break;
			default:
				break;
		}
		getParent().add(node);
	}

	public class BlockOutOfMatrixException extends Exception {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1225977144710417090L;
		Matrix3D matrix3d;
		
		public BlockOutOfMatrixException(Matrix3D matrix3d) {
			this.matrix3d = matrix3d;
		}
		
		@Override
		public String getMessage() {
			return "O bloco está sendo posicionado fora da matriz.";
		}
		
	}
	
}
