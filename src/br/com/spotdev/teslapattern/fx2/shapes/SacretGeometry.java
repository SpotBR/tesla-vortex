package br.com.spotdev.teslapattern.fx2.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class SacretGeometry 
{
	public static final Color[] color_list = new Color[] 
			{Color.YELLOW, Color.MAGENTA/*, Color.BLUE, Color.ORANGE, Color.yellow, Color.PINK*/};
	public static int selectedValue = 12;
    public static final int[] nList = {3,6,9,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,19,18,19,20,21,22,23,24,25,26,27};
//    public static final int[] nList = {3,9,15,21,27,33,39};
    public static int selectedIndex = 0;
    public static Color VORTEX_COLOR = Color.BLACK;
    public static Color LINE_COLOR = Color.GREEN;
    public static int COLUMN_LENGHT = 6;
    public static int NUMBERS_PER_COLUMN = 12;
    public static int RADIUS = 30;
    public static boolean DRAW_ARC = true;
	
    public static void main( String[] args )
    {
    	
//    	double result = 0;
//    	int l = 1;
//    	while(result%1 != 0 || result == 0) {
//    		result = Math.sqrt(Math.pow(l, 2)*2);
//    		System.out.printf("d² = %d²+%d² | d² = %f | d = %f \n",l,l,Math.pow(l, 2)*2,result);
//    		l++;
//    		try {
//				Thread.sleep(50);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//    	}
    	
        JFrame frame = new JFrame();
        frame.setSize(500, 500);
        frame.setTitle("SacretGeometry Pattern");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setAlwaysOnTop(true);
        PatternPanel pp = new PatternPanel();
        frame.add(pp);
        
        frame.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent arg0) {
				
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				
			}
			@Override
			public void keyPressed(KeyEvent arg0) {
				if(arg0.getKeyCode() == KeyEvent.VK_RIGHT) {
					next();
				}else if(arg0.getKeyCode() == KeyEvent.VK_LEFT){ 
					selectedValue--;
					if(selectedValue <= 0)
						selectedValue = 1;
					
					selectedIndex--;
					if(selectedIndex < 0)
						selectedIndex = nList.length-1;
				}else if(arg0.getKeyChar() == "+".toCharArray()[0]) {
					RADIUS+=10;
				}else if(arg0.getKeyChar() == "-".toCharArray()[0]){ 
					RADIUS-=10;
					if(RADIUS <= 0) {
						RADIUS = 1;
					}
				}if(arg0.getKeyChar() == "=".toCharArray()[0]) {
					NUMBERS_PER_COLUMN+=5;
				}else if(arg0.getKeyCode() == KeyEvent.VK_BACK_SPACE){ 
					NUMBERS_PER_COLUMN-=5;
					if(NUMBERS_PER_COLUMN <= 0) {
						NUMBERS_PER_COLUMN = 1;
					}
				}else if(arg0.getKeyCode() == KeyEvent.VK_UP) {
					COLUMN_LENGHT++;
				}else if(arg0.getKeyCode() == KeyEvent.VK_DOWN){ 
					COLUMN_LENGHT--;
					if(COLUMN_LENGHT <= 0)
						COLUMN_LENGHT = 1;
				}else if(arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					VORTEX_COLOR = VORTEX_COLOR == Color.WHITE ? Color.BLACK : Color.WHITE;
				}else if(arg0.getKeyCode() == KeyEvent.VK_SPACE) {
					LINE_COLOR = LINE_COLOR == Color.GREEN ? new Color(0, 0, 0, 0) : Color.GREEN;
				}else if(arg0.getKeyCode() == KeyEvent.VK_ESCAPE) {
					COLUMN_LENGHT = 12;
				}else if(arg0.getKeyCode() == KeyEvent.VK_SHIFT) {
					DRAW_ARC = !DRAW_ARC;
				}else {
					char c = arg0.getKeyChar();
					try {
						int v = Integer.parseInt(c+"");
						COLUMN_LENGHT = v;
					}catch(Exception e) {
						
					}
				}
				
			}
		});
        
        frame.setVisible(true);
    }
    
    public static class PatternPanel extends JPanel {

        /**
         * 
         */
        private static final long serialVersionUID = -5993841174943778557L;
        
        @Override
        protected void paintComponent(Graphics g) {
        	
            super.paintComponent(g);
 
            double frameWidth = g.getClip().getBounds().getWidth();
            double frameHeigth = g.getClip().getBounds().getHeight();
            
            // Background
            g.fillRect(0, 0, (int)frameWidth, (int)frameHeigth);
            
			int selectedNumber = selectedValue;
            g.setColor(VORTEX_COLOR);
            final int colorPerCycle = COLUMN_LENGHT;
            
            final int centerX = (int)frameWidth/2;
            final int centerY = (int)frameHeigth/2;
            
            // Draw numbers
            int LAST_RADIUS = RADIUS;
            Point[] numbersPos = new Point[NUMBERS_PER_COLUMN*COLUMN_LENGHT+1];
            g.setColor(Color.WHITE);
            final int inteiro = 100;
            int divisoes = 1;
            final int seminteiro = (int)(((inteiro*1.414214-inteiro)/divisoes));
            
            g.drawLine(centerX, centerY, centerX+inteiro, centerY);
            g.drawLine(centerX, centerY, centerX, centerY+inteiro);
            
            
            for(int i = 0; i < divisoes; i++) {
            	
            }
            int i = 1;
            for (int cn = 0; cn < COLUMN_LENGHT; cn++) {
                
                int xval = (int) (centerX + COLUMN_LENGHT
                        * Math.cos(cn * 2 * Math.PI / COLUMN_LENGHT));
                int yval = (int) (centerY + COLUMN_LENGHT
                        * Math.sin(cn * 2 * Math.PI / COLUMN_LENGHT));
                numbersPos[i] = new Point(xval, yval);
                
                g.drawString(i+"", xval, yval);
                System.out.println(cn);
                i++;
            }
            
//            g.drawLine(centerX, centerY, (centerX+seminteiro/1), centerY+seminteiro);

            g.drawLine(centerX, centerY, centerX+seminteiro, (centerY+seminteiro/divisoes));
//            g.drawLine(centerX, centerY, (centerX+seminteiro/divisoes), centerY+seminteiro);
            g.drawLine(centerX+seminteiro, (centerY+seminteiro/divisoes), centerX+inteiro, centerY);
            g.drawLine(centerX, centerY+inteiro, centerX+seminteiro, (centerY+seminteiro/divisoes));
//            g.drawLine(centerX+seminteiro, centerY+seminteiro/2, (centerX+seminteiro/2), centerY+seminteiro);

            g.drawLine(centerX, centerY, centerX+seminteiro, centerY-seminteiro/2);
            g.drawLine(centerX, centerY, (centerX+seminteiro/2), centerY-seminteiro);
            g.drawLine(centerX+seminteiro, centerY-seminteiro/2, centerX+inteiro, centerY);
            g.drawLine(centerX+seminteiro, centerY-seminteiro/2, (centerX+seminteiro/2), centerY-seminteiro);
            g.drawLine(centerX+seminteiro/2, centerY-seminteiro, centerX, centerY-inteiro);

            
            g.drawLine(centerX, centerY, centerX-seminteiro, centerY-seminteiro/2);
            g.drawLine(centerX, centerY, (centerX-seminteiro/2), centerY-seminteiro);
            g.drawLine(centerX, centerY, centerX-inteiro, centerY);
            g.drawLine(centerX, centerY, centerX, centerY-inteiro);
            g.drawLine(centerX-seminteiro, centerY-seminteiro/2, centerX-inteiro, centerY);
            g.drawLine(centerX-seminteiro, centerY-seminteiro/2, (centerX-seminteiro/2), centerY-seminteiro);
            g.drawLine(centerX-seminteiro/2, centerY-seminteiro, centerX, centerY-inteiro);

            
//            g.drawLine(centerX, centerY, centerX+seminteiro, centerY+inteiro/2);
//            g.drawLine(centerX, centerY, centerX-inteiro/2, centerY-inteiro+15);
//            g.drawLine(centerX, centerY, centerX-inteiro+15, centerY+inteiro/2);
//            g.drawLine(centerX, centerY, centerX+inteiro, centerY-inteiro);
//            g.drawLine(centerX, centerY, centerX, centerY-inteiro);
//            g.drawLine(centerX, centerY, centerX-inteiro, centerY);
            

//            g.drawString(1+"", centerX+inteiro, centerY);
//            g.drawString(4+"", centerX-inteiro, centerY-inteiro);
//            g.drawString(2+"", centerX-inteiro, centerY+inteiro);
//            g.drawString(3+"", centerX+inteiro, centerY-inteiro);
//            
//            // Measure line
//            g.setColor(Color.RED);
//        	g.drawLine(numbersPos[1].x, numbersPos[1].y, numbersPos[COLUMN_LENGHT].x, numbersPos[COLUMN_LENGHT].y);
//            
//            // Draw center to border lines
//            g.setColor(Color.WHITE);
//            for(int a = 0; a < COLUMN_LENGHT; a++) {
//            	Point current = numbersPos[((COLUMN_LENGHT*NUMBERS_PER_COLUMN)-a)];
//            	g.drawLine(centerX, centerY, current.x, current.y);
//            }
//            
//            // Draw arc
//            int cycle = 0;
//            int colorIndex = 0;
//            g.setColor(color_list[0]);
//            if(DRAW_ARC) {
//	            for(int n = 1; n < numbersPos.length-1; n++) {
//	            	cycle++;
//	            	Point p = numbersPos[n];
//	            	Point p2 = numbersPos[n+1];
//	            	
//	            	if(cycle >= colorPerCycle) {
//	
//	        			colorIndex++;
//	            		if(colorIndex >= color_list.length) {
//	            			colorIndex = 0;
//	            		}
//	            		cycle = 0;
//	                    g.setColor(color_list[colorIndex]);
//	            	}
//	            		
//	            	
//	            	g.drawLine((int)p.getX(), (int)p.getY(), 
//	            			(int)p2.getX(), (int)p2.getY());
//	            }
//            }
//            
//            
//            
//            // Draw lines
//            Point last = null;
//	    		for(int v = 1; v < numbersPos.length; v++) {
//	                g.setColor(LINE_COLOR);
//	            	if(v%selectedNumber == 0) {
//		            	Point nPoint = numbersPos[v];
//		            	if(last != null) {
//		            		g.drawLine((int)last.getX(), (int)last.getY(), 
//		            				(int)nPoint.getX(), (int)nPoint.getY());
//		            	}
//	
//	            		last = nPoint;
//	            	}
//	        	}
//            
//    		g.setColor(Color.white);
//    		g.setFont(new Font("Arial", 0, 70));
//    		g.drawString("NUM: "+selectedNumber+"", 10, 70);
//    		g.drawString("COL: "+COLUMN_LENGHT+"", 10, 150);
//    		g.drawString("ZOOM: "+RADIUS, 10, (int)(frameHeigth-90));
//    		g.drawString("DIS: "+(numbersPos[1].distance(numbersPos[COLUMN_LENGHT]))+"", 10, (int)(frameHeigth-20));
//    	
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//            next();
            super.repaint();
            
        }
        
    }
    
    private static void drawLine(int x, int y, int xto, int yto) {
    	
    }
    
    private static void next() {
    	selectedIndex++;
    	selectedValue+=1;
		if(selectedIndex >= nList.length)
			selectedIndex = 0;
    }
    
    
}
