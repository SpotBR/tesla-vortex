package br.com.spotdev.teslapattern;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import br.com.spotdev.teslapattern.fx2d.PlaceholderTextField;
import br.com.spotdev.teslapattern.fx2d.Vec2f;
import br.com.spotdev.teslapattern.fx3d.shapes.Matrix3D.BlockOutOfMatrixException;

public class TeslaPattern2D 
{
	public static final Font ARIAL_70 = new Font("Arial", 0, 70);
	public static final Font ARIAL_40 = new Font("Arial", 0, 40);
	public static final Vec2f CENTER = new Vec2f();
	public static final Color[] color_list = new Color[] 
			{Color.RED, Color.MAGENTA/*, Color.BLUE, Color.ORANGE, Color.yellow, Color.PINK*/};
	public static int selectedValue = 1;
    public static final int[] nList = {3,6,9,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,19,18,19,20,21,22,23,24,25,26,27};
//    public static final int[] nList = {3,9,15,21,27,33,39};
    public static int selectedIndex = 0    ;
    public static Color VERTEX_COLOR = Color.white;
    public static Color LINE_COLOR = Color.GREEN;
    public static int COLUMN_LENGHT = 12;
    public static int NUMBERS_PER_COLUMN = 12;
    public static float RADIUS = 30;
    public static boolean DRAW_ARC = true;
    public static boolean SUM_DIGIT = false;
    public static boolean MIRROR = false;
    public static int SHOW_NUMBERS = 0;
    public static boolean SHOW_MENU = true;
    public static int MOVE_VORTEX_X = 0;
    public static int MOVE_VORTEX_Y = 0;
    public static int[] CUSTOM_SEQUENCE = {1,4,8,9,1};
    public static 
    
    enum OrderType {
    	NEAREST_Vec2f,
    	NUMBER_ORDER
    	
    }
    public static OrderType ORDER_TYPE = OrderType.NUMBER_ORDER;
    
    // ANIMATION
    public static boolean ANIMATED = false;
    public static long ANIMATION_STEP_DELAY = 0;
    public static int ANIMATION_STEP = 0;
    public static int ANIMATION_NUMBER_TO_CHECK = 0;
    
    // Panel 
   public static PlaceholderTextField CUSTOM_SEQUENCE_FIELD = null;
   public static JFrame FORM = null;
    

	private static final Set<Integer> pressed = new HashSet<Integer>();
	
    public static long fibo(int n) {
        if (n < 2) {
            return n;
        } else {
            return fibo(n - 1) + fibo(n - 2);
        }
    }
 
	
    public static void main( String[] args )
    {
    	CUSTOM_SEQUENCE = new int[200];
    	int i2 = 0;
		for(int i = 1; i < CUSTOM_SEQUENCE.length; i++) {
			i2+=i;
			CUSTOM_SEQUENCE[i-1] = i2-1;
		}
    	
        FORM = new JFrame();
        FORM.setSize(500, 500);
        FORM.setTitle("Tesla 369 Pattern v3.8");
        FORM.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        FORM.setExtendedState(JFrame.MAXIMIZED_BOTH);
        PatternPanel pp = new PatternPanel();
        FORM.add(pp);
        
        
        FORM.addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowIconified(WindowEvent e) {
				pressed.clear();
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {
				pressed.clear();
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
        
	    FORM.addMouseWheelListener(new MouseWheelListener() {
			
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				int notches = e.getWheelRotation();
				if(notches < 0) {
					RADIUS+=2;
				}else if(notches > 0){ 
					RADIUS-=2;
					if(RADIUS <= 0) {
						RADIUS = 1;
					}
				}
				
				if(notches != 0) {
					FORM.repaint();
				}
			}
		});
        
        FORM.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
		        pressed.remove(arg0.getKeyCode());
			}
			@Override
			public void keyPressed(KeyEvent arg0) {

		        pressed.add(arg0.getKeyCode());
		        if (pressed.size() == 2) {
		        	if(pressed.contains(KeyEvent.VK_SHIFT)) {
		        		if(arg0.getKeyCode() == KeyEvent.VK_SPACE) {
							DRAW_ARC = !DRAW_ARC;
						}else {
							if(arg0.getKeyCode() == KeyEvent.VK_RIGHT) 
								MOVE_VORTEX_X+=10;
							if(arg0.getKeyCode() == KeyEvent.VK_LEFT)
								MOVE_VORTEX_X-=10;
							if(arg0.getKeyCode() == KeyEvent.VK_UP)
								MOVE_VORTEX_Y-=10;
							if(arg0.getKeyCode() == KeyEvent.VK_DOWN)
								MOVE_VORTEX_Y+=10;
						}
		        	}
		        }else if(pressed.size() == 1) {
					if(arg0.getKeyCode() == KeyEvent.VK_F1) {
						SHOW_MENU = !SHOW_MENU;
					}if(arg0.getKeyCode() == KeyEvent.VK_F2) {
						CUSTOM_SEQUENCE_FIELD.setVisible(!CUSTOM_SEQUENCE_FIELD.isVisible());
					}else if(arg0.getKeyCode() == KeyEvent.VK_SEMICOLON) {
						SHOW_NUMBERS++;
						if(SHOW_NUMBERS > 2)
							SHOW_NUMBERS = 0;
					}else if(arg0.getKeyCode() == KeyEvent.VK_RIGHT) {
						next();
						resetAnimation();
					}else if(arg0.getKeyCode() == KeyEvent.VK_LEFT){ 
						selectedValue--;
						if(selectedValue <= 0)
							selectedValue = 0;
						
						selectedIndex--;
						if(selectedIndex < 0)
							selectedIndex = nList.length-1;
						resetAnimation();
					}else if(arg0.getKeyChar() == "+".toCharArray()[0] || arg0.getKeyChar() == "=".toCharArray()[0]) {
						RADIUS+=1;
					}else if(arg0.getKeyChar() == "-".toCharArray()[0]){ 
						RADIUS-=1;
						if(RADIUS <= 0) {
							RADIUS = 1;
						}
					}if(arg0.getKeyCode() == KeyEvent.VK_PERIOD) {
						NUMBERS_PER_COLUMN+=1;
					}else if(arg0.getKeyCode() == KeyEvent.VK_COMMA){ 
						NUMBERS_PER_COLUMN-=1;
						if(NUMBERS_PER_COLUMN <= 0) {
							NUMBERS_PER_COLUMN = 1;
						}
						resetAnimation();
					}else if(arg0.getKeyCode() == KeyEvent.VK_UP) {
						COLUMN_LENGHT++;
						resetAnimation();
					}else if(arg0.getKeyCode() == KeyEvent.VK_DOWN){ 
						COLUMN_LENGHT--;
						if(COLUMN_LENGHT <= 0)
							COLUMN_LENGHT = 1;
						resetAnimation();
					}else if(arg0.getKeyCode() == KeyEvent.VK_ENTER) {
						VERTEX_COLOR = VERTEX_COLOR == Color.WHITE ? Color.darkGray : Color.WHITE;
					}else if(arg0.getKeyCode() == KeyEvent.VK_SPACE) {
						LINE_COLOR = LINE_COLOR == Color.GREEN ? new Color(0, 0, 0, 0) : Color.GREEN;
					}else if(arg0.getKeyCode() == KeyEvent.VK_ESCAPE) {
						COLUMN_LENGHT = 12;
						MOVE_VORTEX_X = 0;
						MOVE_VORTEX_Y = 0;
						RADIUS = 30;
						resetAnimation();
					}else if(arg0.getKeyCode() == KeyEvent.VK_INSERT) {
						SUM_DIGIT = !SUM_DIGIT;
						resetAnimation();
					}else if(arg0.getKeyCode() == KeyEvent.VK_M) {
						MIRROR = !MIRROR;
						resetAnimation();
					}else if(arg0.getKeyCode() == KeyEvent.VK_N){
						resetAnimation();
						ANIMATED = !ANIMATED;
					}else{
						char c = arg0.getKeyChar();
						try {
							int v = Integer.parseInt(c+"");
							if(v != 0)
								COLUMN_LENGHT = v;
						}catch(Exception e) {
							
						}
					}
		        }
				FORM.repaint();
			}
		});
        
        FORM.setVisible(true);
        
//        new Thread(new Runnable() {
//			
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				while(true) {
//					try {
//						Thread.sleep(100);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					synchronized (FORM) {
//						selectedValue++;
//						FORM.repaint();
//					}
//				}
//				
//			}
//		}).start();
    }
    
    public static class PatternPanel extends JPanel {

        /**
         * 
         */
        private static final long serialVersionUID = -5993841174943778557L;
        
        
        public PatternPanel() {
        	
        	Rectangle r = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        	
        	this.setLayout(new BorderLayout());
        	JPanel customSequencePanel = new JPanel(new BorderLayout());
        	customSequencePanel.setPreferredSize(new Dimension((int) r.getWidth(), 100));
        	customSequencePanel.setBackground(new Color(94,94,94,0));
        	this.add(customSequencePanel,BorderLayout.NORTH);
        	PatternPanel patternPanel = this;
        	
        	CUSTOM_SEQUENCE_FIELD = new PlaceholderTextField();
        	CUSTOM_SEQUENCE_FIELD.setPlaceholder("Insira a sequência customizada aqui");
        	CUSTOM_SEQUENCE_FIELD.setSize((int) r.getWidth(), 100);
        	customSequencePanel.add(CUSTOM_SEQUENCE_FIELD,BorderLayout.CENTER);
        	
        	CUSTOM_SEQUENCE_FIELD.addKeyListener(new KeyListener() {
				
				@Override
				public void keyTyped(KeyEvent arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void keyReleased(KeyEvent arg0) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void keyPressed(KeyEvent arg0) {
					if(arg0.getKeyCode() == KeyEvent.VK_ENTER) {
						CUSTOM_SEQUENCE_FIELD.setText(CUSTOM_SEQUENCE_FIELD.getText().trim().replaceAll("[^\\d.]", ""));
						if(getSequenceValue() != null) {
							CUSTOM_SEQUENCE = getSequenceValue();
							selectedValue = -1;
							customSequencePanel.repaint();
						}else {
							JOptionPane.showConfirmDialog(null, "Sequência inválida. Insira apenas números.", 
									"Erro", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
						}

						CUSTOM_SEQUENCE_FIELD.setVisible(false);
						customSequencePanel.repaint();
						patternPanel.repaint();
						FORM.requestFocus();
					}
					
				}
			});


        	CUSTOM_SEQUENCE_FIELD.setVisible(false);
        	
		}
        
        @Override
        protected void paintComponent(Graphics g) {

            super.paintComponent(g);

            float frameWidth = (float)g.getClip().getBounds().getWidth();
            float frameHeigth = (float)g.getClip().getBounds().getHeight();
            
            // Background
            g.setColor(Color.DARK_GRAY);
            g.fillRect(0, 0, (int)frameWidth, (int)frameHeigth);
            
//            boolean yes = true;
//            for(int ci = 0; ci < frameWidth; ci+=1) {
//            	for(int ca = 0; ca < frameHeigth; ca+=1) {
//                	if(yes) {
//                		g.setColor(new Color(225, 0, 225));
//                	}else {
//                		g.setColor(new Color(255, 0, 0));
//                	}
//            		g.drawRect(ca, ci, 1, 1);
//                	yes  = !yes;
//                }
//
//            	yes  = !yes;
//            }
//            
//            if(yes == false || yes == true) return;
			int selectedNumber = selectedValue;
			
            final int colorPerCycle = COLUMN_LENGHT;
            
            final int centerX = (int)frameWidth/2+MOVE_VORTEX_X;
            final int centerY = (int)frameHeigth/2+MOVE_VORTEX_Y;

            CENTER.x = centerX;
            CENTER.y = centerY;
            
            // Draw numbers
            int i = 1;
            float LAST_RADIUS = RADIUS;
            Vec2f[] numbersPos = new Vec2f[NUMBERS_PER_COLUMN*COLUMN_LENGHT+1];
            Vec2f[] negativeNumbersPos = new Vec2f[NUMBERS_PER_COLUMN*COLUMN_LENGHT+1];
            for(int nb = 0; nb < NUMBERS_PER_COLUMN; nb++) {
                for (int cn = 0; cn < COLUMN_LENGHT; cn++) {
                    float RADIUSUsed = (LAST_RADIUS+RADIUS);
                    LAST_RADIUS = RADIUSUsed;

                    int summedDigit = sumDigit(i);
                    int digit = SUM_DIGIT ? summedDigit : i;  
                    if(summedDigit == 3 || summedDigit == 6 || summedDigit == 9) {
                    	g.setColor(Color.GREEN);
                    }else if(summedDigit == 1 || summedDigit == 4 || summedDigit == 7){
                    	g.setColor(Color.CYAN);
                    }else{
                    	g.setColor(Color.white);
                    }
                    
                    
                    if(isPrime(i))
                		g.setFont(new Font("Arial", Font.BOLD, 18));
                    else
                		g.setFont(new Font("Arial", 0, 15));
                    
//                    int xval = (int) (centerX + RADIUSUsed/COLUMN_LENGHT
//                            * Math.cos(cn * 2 * Math.PI / COLUMN_LENGHT));
//                    int yval = (int) (centerY + RADIUSUsed/COLUMN_LENGHT
//                            * Math.sin(cn * 2 * Math.PI / COLUMN_LENGHT));
                    
                    System.out.println(cn * 2 * Math.PI / COLUMN_LENGHT);
                    float xval = (float)(centerX + RADIUSUsed/COLUMN_LENGHT
                            * Math.cos(cn * 2 * Math.PI / COLUMN_LENGHT));
                    float yval = (float)(centerY + RADIUSUsed/COLUMN_LENGHT
                            * Math.sin(cn * 2 * Math.PI / COLUMN_LENGHT));
                    numbersPos[i] = new Vec2f(xval, yval);
                        
                    
                    if(SHOW_NUMBERS == 0 || (SHOW_NUMBERS == 2 && isPrime(i)))
                    	g.drawString(digit+"", (int)xval, (int)yval);
                    
                    if(MIRROR) {
	                    xval = (float) (centerX -	 RADIUSUsed/COLUMN_LENGHT
	                            * Math.cos(cn * 2 * Math.PI / COLUMN_LENGHT));
	                    yval = (float) (centerY - RADIUSUsed/COLUMN_LENGHT
	                            * Math.sin(cn * 2 * Math.PI / COLUMN_LENGHT));
	                    negativeNumbersPos[i] = new Vec2f(xval, yval);
	                    if(SHOW_NUMBERS == 0 || (SHOW_NUMBERS == 2 && isPrime(i)))
	                    	g.drawString(digit+"", (int)xval, (int)yval);
                    }
                    
                    i++;
                }
            }
            
            g.setColor(Color.YELLOW);
            drawDashedLine(g,(int)numbersPos[1].x, (int)numbersPos[1].y, (int)numbersPos[COLUMN_LENGHT].x, (int)numbersPos[COLUMN_LENGHT].y,1);
            
            // Draw center to border lines
        	g.setColor(VERTEX_COLOR);
            for(int a = 0; a < COLUMN_LENGHT; a++) {
            	Vec2f current = numbersPos[((COLUMN_LENGHT*NUMBERS_PER_COLUMN)-a)];
            //	drawDashedLine(g,(int)centerX, (int)centerY, (int)current.x, (int)current.y,1);
            }
            
            // Draw arc
            int cycle = 0;
            int colorIndex = 0;
            g.setColor(color_list[0]);
            if(DRAW_ARC) {
	            for(int n = 1; n < numbersPos.length-1; n++) {
	            	cycle++;
	            	Vec2f p = numbersPos[n];
	            	Vec2f p2 = numbersPos[n+1];
	            	
	            	Vec2f np = negativeNumbersPos[n];
	            	Vec2f np2 = negativeNumbersPos[n+1];
	            	
	            	if(cycle >= colorPerCycle) {
	        			colorIndex++;
	            		if(colorIndex >= color_list.length) {
	            			colorIndex = 0;
	            		}
	            		cycle = 0;
	                    g.setColor(color_list[colorIndex]);
	            	}
	            		
	            	
	            	g.drawLine((int)p.x, (int)p.y, 
	            			(int)p2.x, (int)p2.y);
	            	
		            if(MIRROR) {
		            	g.drawLine((int)np.x, (int)np.y, 
		            			(int)np2.x, (int)np2.y);
		            }
	            }
            }
            

            List<Vec2f> points = new ArrayList<Vec2f>();
            List<Vec2f> negativeVec2fs = new ArrayList<Vec2f>();
            
            for(int z = 1; z < numbersPos.length; z++) {
	        	if((selectedValue == -1 || selectedNumber == 0 && isPrime(z)) ||
	        			(selectedNumber > 0 && z%selectedNumber == 0)) {
	        		points.add(numbersPos[z]);
	        	}
            }
            
            if(ORDER_TYPE == OrderType.NEAREST_Vec2f)
            	points = getNearestList(points);
            
            // Draw lines
            Vec2f last = null;
            Vec2f negativelast = null;
            int customCycles = 0;
    		
    		for(int v = 0; v < points.size(); v++) {	
    			// Sistema de sequência customizada
    			int customLenght = CUSTOM_SEQUENCE.length;
    			if(selectedValue == -1) {
    				int n = CUSTOM_SEQUENCE[customCycles];

        			if(v == n) {
        				customCycles++;
        				if(customCycles >= customLenght)
        					customCycles = 0;
        			}else
        				continue;
    			}
    			
    			
    			
                g.setColor(LINE_COLOR);
            	Vec2f nVec2f = points.get(v);
            	Vec2f negativeVec2f = null;
            	if(MIRROR) negativeVec2f = negativeNumbersPos[v];
            	
            	if(last != null) {
            		if(ANIMATION_NUMBER_TO_CHECK == 0)
            			ANIMATION_NUMBER_TO_CHECK = v;

            		if(ANIMATION_NUMBER_TO_CHECK >= v || !ANIMATED) {
            			int step = ANIMATION_NUMBER_TO_CHECK == v && ANIMATED ? ANIMATION_STEP : 100;
	            		g.drawLine((int)last.x, (int)last.y, 
	            				(int)(((nVec2f.x-last.x)*
	            						((double)step/100))+last.x), 
	            				(int)(((nVec2f.y-last.y)*((double)step/100))+last.y));
	            		
	            		if(MIRROR) {
	            			g.drawLine((int)negativelast.x, (int)negativelast.y, 
	            				(int)(((negativeVec2f.x-negativelast.x)*
	            						((double)step/100))+negativelast.x), 
	            				(int)(((negativeVec2f.y-negativelast.y)*((double)step/100))+negativelast.y));
	            			negativeVec2fs.add(negativeVec2f);
	            		}

	            		if(ANIMATION_NUMBER_TO_CHECK == v)
		            		if(ANIMATED && ANIMATION_STEP != 100 && System.currentTimeMillis()-ANIMATION_STEP_DELAY >= 10) {
			            		ANIMATION_STEP+=5;
			            		ANIMATION_STEP_DELAY = System.currentTimeMillis();
		            		}
            		}else if(ANIMATION_STEP == 100) {
            			ANIMATION_NUMBER_TO_CHECK=v;
            			ANIMATION_STEP = 0;
	            		ANIMATION_STEP_DELAY = System.currentTimeMillis();
            		}
            	}

        		last = nVec2f;
        		negativelast = negativeVec2f;
            	
        	}
            points.clear();
            points = null;
    		g.setColor(Color.white);
    		

    		g.setFont(ARIAL_70);
    		String num = null;
    		switch(selectedNumber) {
    		case 0:
    			num = "PRIMOS";
    			break;
    		
    		case -1:
    			num = getSequenceString();
    			break;
    		default:
    			num = selectedNumber+"";
    		}
    		g.drawString("NUM: "+num +"", 10, 70);
    		g.drawString("COL: "+COLUMN_LENGHT+"", 10, 150);
    		if(selectedNumber > 0)
    			g.drawString("PROP: "+asFraction(selectedNumber, COLUMN_LENGHT)+"", 10, 230);
    		g.drawString("ZOOM: "+RADIUS, 10, (int)(frameHeigth-90));
    		g.drawString("DIS: "+(numbersPos[1].distance(numbersPos[COLUMN_LENGHT]))+" - CDIS: "+(CENTER.distance(numbersPos[COLUMN_LENGHT])), 10, (int)(frameHeigth-20));
    		
    		if(SHOW_MENU) {
	    		int helpMenuX = (int)frameWidth-500;
	    		int helpMenuY = 40;
	    		g.setColor(new Color(0,0,0,100));
	    		g.fillRect(helpMenuX, helpMenuY-15, 470, 750);

	    		g.setColor(Color.white);
	    		g.drawLine(helpMenuX+240, helpMenuY+10, helpMenuX+240, 750);
	    		
	    		g.setFont(ARIAL_40);
	    		g.drawString("Comandos (F1 para fechar)", helpMenuX, helpMenuY);
	
	    		drawString(g, "SETAS   ↑   ↓:\n  Altera a quantidade de vértices\nno vórtice.", helpMenuX+10, helpMenuY+10);
	    		drawString(g, "SETAS   ←   →:\n Altera o número usado para\nse fazer o traçado ou muda para\no modo PRIMOS.", helpMenuX+10, helpMenuY+80);
	    		drawString(g, "ESPAÇO:\n Faz aparecer ou desaparecer\nos TRAÇADOS VERDES.", helpMenuX+10, helpMenuY+170);
	    		drawString(g, "SHIFT+ESPAÇO:\n Faz aparecer ou desaparecer\no vórtice.", helpMenuX+10, helpMenuY+240);
	    		drawString(g, "VÍRGULA (,) E PONTO (.):\n Aumenta ou diminue a\nquantidade de camadas no\nvórtice.", helpMenuX+10, helpMenuY+310);
	    		drawString(g, "MAIS (+) E MENOS (-):\n Aumenta e diminue o ZOOM", helpMenuX+10, helpMenuY+400);
	    		drawString(g, "LETRA M:\n Ativa e desativa o modo de\nvórtice espelhado", helpMenuX+10, helpMenuY+460);
	    		drawString(g, "ENTER:\n Oculta as linhas do centro\naos vértices.", helpMenuX+10, helpMenuY+540);
	    		
	    		drawString(g, "F1:\n Faz aparecer e desaparecer\nos informativos.", helpMenuX+260, helpMenuY+10);
	    		drawString(g, "ESC:\n Reseta a posição do vórtice\ne o zoom.", helpMenuX+260, helpMenuY+90);
	    		drawString(g, "INSERT:\n Soma os dígitos dos números\nmaiores que NOVE.", helpMenuX+260, helpMenuY+170);
	    		drawString(g, "PONTO E VÍRGULA (;):\n Faz aparecer e\ndesaparecer os números.", helpMenuX+260, helpMenuY+240);
	    		drawString(g, "N:\n Ativa ou desativa a animação.", helpMenuX+260, helpMenuY+310);
	    		drawString(g, "SHIFT + ↑  ↓  ←  →:\n Move o vórtice para os lados.", helpMenuX+260, helpMenuY+370);
	    		drawString(g, "F2:\n Ativa o modo de sequência\ncustomizada.", helpMenuX+260, helpMenuY+420);
    		}
    	
 
            if(ANIMATED) {
            	repaint();
            }
    		
        }
        
        private int[] getSequenceValue() {
        	String data = CUSTOM_SEQUENCE_FIELD.getText();
        	String[] numbers = data.split("");
        	int[] numbersInteger = new int[numbers.length];
        	for(int i = 0; i < numbers.length; i++) {
        		try {
        			numbersInteger[i] = Integer.parseInt(numbers[i]);
        		}catch(NumberFormatException e) {
        			return null;
        		}
        	}
        	
        	return numbersInteger;
        	
        }
        
        private String getSequenceString() {
        	StringBuilder sb = new StringBuilder();
        	sb.append(CUSTOM_SEQUENCE[0]);
        	for(int i = 1; i < CUSTOM_SEQUENCE.length; i++) {
        		sb.append(", ");
        		sb.append(CUSTOM_SEQUENCE[i]);
        	}
        	return sb.toString();
        }
        
    }
    
    public static List<Vec2f> getNearestList(List<Vec2f> numberPos) {
    	
    	List<Vec2f> sortedList = new ArrayList<Vec2f>();
    	sortedList.addAll(numberPos);
    	List<Vec2f> nearestList = new ArrayList<Vec2f>();
    	nearestList.add(sortedList.get(0));
    	// O problema de usar só dois loop é que o segundo número não
    	// vai ser o segundo da lista mas sim o último mais próximo encontrado
    	// Sendo assim, se eu quiser usar fazer com que a cada numero, eu meça a distância, 
    	// e em seguida pegue o próximo, será preciso de um while que resgate um número de antes da lista 
    	// que foi pulado. Logo, preciso de uma lista só para armazenar os números que ainda são necessarios. 
    	//int i = 0;
    	while(sortedList.size() != 0){
    		//i++;
    		Vec2f currentVec2f = nearestList.size() == 0 ? sortedList.get(0) : nearestList.get(nearestList.size()-1);
    		//System.out.println(i+" - De todos esses:");
    		Vec2f moreClose = sortedList.get(0);
	    	for(int x = 1; x < sortedList.size(); x++) {
	    		Vec2f indexVec2f = sortedList.get(x);
    			//System.out.println("\t"+indexVec2f.x+"|"+indexVec2f.y+" - "+currentVec2f.distance(indexVec2f));
	    		if(((currentVec2f.distance(moreClose) > currentVec2f.distance(indexVec2f))) 
	    				|| moreClose == currentVec2f) {
	    			moreClose = indexVec2f;
	    		//	System.out.println("\t"+indexVec2f.x+"|"+indexVec2f.y+" - "+currentVec2f.distance(indexVec2f));
	    		}
	    	}
	    	sortedList.remove(moreClose);
	    	nearestList.add(moreClose);
//	    	System.out.println("\tO mais próximo é "+currentVec2f.distance(moreClose)+" - "+currentVec2f+" - "+moreClose);    	
//	    	System.out.println("\tLista:");
	    	nearestList.forEach((v)->{;
	    		System.out.println("\t"+v);
	    	});
    	}
//    	System.out.println(nearestList.size()+" - "+sortedList.size());
    	sortedList.clear();
    	sortedList = null;
    	System.gc();
    	return nearestList;
    }
    
    public static void drawDashedLine(Graphics g, int x1, int y1, int x2, int y2, int w){

        //creates a copy of the Graphics instance
        Graphics2D g2d = (Graphics2D) g.create();

        //set the stroke of the copy, not the original 
        Stroke dashed = new BasicStroke(w, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0);
        g2d.setStroke(dashed);
        g2d.drawLine(x1, y1, x2, y2);

        //gets rid of the copy
        g2d.dispose();
    }
    
    public static void drawString(Graphics g, String text, int x, int y) {
    	String[] lines = text.split("\n");
        for (int i = 0; i < lines.length; i++) {
        	String line = lines[i];
        	if(i == 0)
        		g.setFont(new Font("Arial", Font.BOLD, 15));
        	else
        		g.setFont(new Font("Arial", 0, 15));
        		
            g.drawString(line, x, y += g.getFontMetrics().getHeight());
        }
    }
    
    private static int sumDigit(int digit) {
    	String dS[] = (digit+"").split("");
    	if(dS.length > 1) {
	    	int result = 0;
	    	while(result == 0 || result >= 10) {
	    		result = 0;
	    		for(int i = 0; i < dS.length; i++) {
	    			result+=Integer.parseInt(dS[i]);
	    		}
    	    	dS = (result+"").split("");
	    	}
	    	return result;
    	}else {
    		return Integer.parseInt(dS[0]);
    	}
    }
    
    static void resetAnimation() {
    	ANIMATION_STEP = 0;
    	ANIMATION_STEP_DELAY = System.currentTimeMillis();
    	ANIMATION_NUMBER_TO_CHECK = 0;
    }
    
    static boolean isPrime(int n) 
    { 
        // Corner case 
        if (n <= 1) 
            return false; 
  
        // Check from 2 to n-1 
        for (int i = 2; i < n; i++) 
            if (n % i == 0) 
                return false; 
  
        return true; 
    }
    
    private static void next() {
    	selectedIndex++;
    	selectedValue+=1;
		if(selectedIndex >= nList.length)
			selectedIndex = 0;
    }
    
    public static long gcm(long a, long b) {
        return b == 0 ? a : gcm(b, a % b); // Not bad for one line of code :)
    }

    public static String asFraction(long a, long b) {
        long gcm = gcm(a, b);
        return (a / gcm) + "/" + (b / gcm);
    }

}
