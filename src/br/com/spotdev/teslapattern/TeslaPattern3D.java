package br.com.spotdev.teslapattern;

import br.com.spotdev.teslapattern.fx3d.World;
import br.com.spotdev.teslapattern.fx3d.shapes.Matrix3D;
import br.com.spotdev.teslapattern.fx3d.shapes.MatrizDeRicardo;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.transform.Translate;
import javafx.stage.Stage;

public class TeslaPattern3D extends Application {

	Matrix3D matrix = null;
	World world = null;
	
	@Override
	public void start(Stage primaryStage) {
		
		world = new World(primaryStage); 
		
		Scene scene = new Scene(world, 1024, 700, true);
		scene.setFill(Color.rgb(250, 250, 250));
		primaryStage.setTitle("3D Matrix Software - By Davi Salles");
		primaryStage.setScene(scene);
		primaryStage.show();

		//root.getChildren().add(createConnection(new Point3D(0, 0, 0), new Point3D(1000, 1000, 1000)));
		Matrix3D matrix = new Matrix3D(40, 40, 1, 200, world.getChildren());
		matrix.generateGrid();
		MatrizDeRicardo.generate(matrix);
		Translate t = new Translate(0, matrix.getTotalPixelsHeight()+matrix.getCubeSideLenght(), 0);
		
		world.getChildren().add(world.getpCamera().buildCamera(matrix));
        world.getLight().getTransforms().add(t);
        world.addWorldEvents(scene);

        

	}
	
	public static void main(String[] args) {
		launch(args);
	}

}
